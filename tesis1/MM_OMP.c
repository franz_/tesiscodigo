
#include <omp.h>
#include <stdio.h>
#include <stdlib.h>
#include "../thirdParty/timer.h"

double* reservarM(int n)
{
    double* M = (double*) malloc(n*n*sizeof(double));
    int i,j;
    for(i=0; i<n;i++)
    {
        for(j=0; j<n;j++)
        {
            M[n*i+j] = 0;
        }
    }
    return M;
}

double* generarM(int n)
{
    double* M = reservarM(n);
    int i,j;
    for(i=0; i<n;i++)
    {
        for(j=0; j<n;j++)
        {
            M[n*i+j] = i+j;
        }
    }
    return M;
}

double MM(double* a, double* b, int n)
{
    double* c = reservarM(n);
    int i,j,k,chunk = 10;
    StartTimer();
    #pragma omp parallel shared(a,b,c,n,chunk) private(i,j,k)
    {
      #pragma omp for schedule (static, chunk)
      for (i=0; i<n; i++)
      {
          for(j=0; j<n; j++)
          {   
              for (k=0; k<n; k++)
              {
                  c[n*i+j]+=a[n*i+k]+b[n*k+j];
              }
          }
      }
    }
    double runtime = GetTimer();
    free(c);
    return runtime/1000.f;
}

int main (int argc, char *argv[]) 
{
    int n[9] = {10,50,100,250,500,750,1000,5000,10000};
    int i=0;
    for(i=0;i<7;i++)
    {
        double* a = generarM(n[i]);
        double* b = generarM(n[i]);
        double runtime = MM(a,b,n[i]);
        printf("%g ",runtime);
        free(a);
        free(b);
    }
    puts("");    
    return EXIT_SUCCESS;
}
