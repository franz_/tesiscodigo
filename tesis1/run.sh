#!/bin/bash
#MM secuencial
gcc MM.c -o MM
./MM >> tEjecucion.txt
echo "results of MM are ready!"

#MM OMP
gcc MM_OMP.c -fopenmp -o MM_OMP
./MM_OMP >> tEjecucion.txt
echo "results of MM_OMP are ready!"

#MM ACC
pgcc -acc MM_ACC.c -o MM_ACC
./MM_ACC >> tEjecucion.txt
echo "results of MM_ACC are ready!"

octave analisis.m
echo "analisis is ready!"
