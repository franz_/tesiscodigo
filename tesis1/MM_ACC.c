#include <stdio.h>
#include <stdlib.h>
#include <openacc.h>
#include "../thirdParty/timer.h"

double* reservarM(int n)
{
    double* M = (double*) malloc(n*n*sizeof(double));
    int i,j;
    for(i=0; i<n;i++)
    {
        for(j=0; j<n;j++)
        {
            M[n*i+j] = 0;
        }
    }
    return M;
}

double* generarM(int n)
{
    double* M = reservarM(n);
    int i,j;
    for(i=0; i<n;i++)
    {
        for(j=0; j<n;j++)
        {
            M[n*i+j] = i+j;
        }
    }
    return M;
}


double matrix_mul( double *a, double *b, int n){
    double* c =reservarM(n);
    StartTimer();
    #pragma acc data copyin(a[0:n*n], b[0:n*n]) copyout(c[0:n*n])
    {
        # pragma acc region
        {
            for (int i = 0; i < n; i ++)
            {    
                # pragma acc loop independent vector(16) 
                for (int j = 0; j < n ; j ++)
                {
                    for (int k = 0; k < n ; k ++)
                    {
                        c[i*n +j] += a[i*n+k]*b[k*n+j*n];
                    }
                }
            }
        }
    }
    double runtime = GetTimer();
    free(c);
    return runtime/1000.f; //tiempo de ejecucion en segundos
}

int main()
{
    int n[9] = {10,50,100,250,500,750,1000,5000,10000};
    int i=0;
    for(i=0;i<7;i++)
    {
        double* a = generarM(n[i]);
        double* b = generarM(n[i]);
        double runtime = matrix_mul(a,b,n[i]);
        printf("%g ",runtime);
        free(a);
        free(b);
    }
    puts("");
    return EXIT_SUCCESS;
}
