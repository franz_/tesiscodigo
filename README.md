# Seminarios Info #

Codigos para seminario de Tesis I y II

Test de Multiplicación de Matrices en OpenMP y OpenACC, adicional algunos scripts y código en Matlab para su posterior análisis.

Método de Newton-Raphson en OpenMP y OpenACC, usando OpenBLAS.