//pgcc -acc -Minfo -lm -lopenblas 

#include <stdio.h>
#include <stdlib.h>
#include <cblas.h>
#include <math.h>

const double rTol = 1e-10;
const long MaxIt = 1e6;

#define TEST_ACC

#ifdef TEST_ACC

#include <openacc.h>
#include "../thirdParty/timer.h"
#include "../thirdParty/lu.h"

int n=0;
int chunk = 10;

double* generarM()
{
    double* v = (double*) malloc(n*n*sizeof(double));
    int i,j;
    #pragma acc data copyout(v[0:n*n])
    {
        #pragma acc region
        {
            #pragma acc loop independent vector(16)
            for(i=0; i<n;i++)
            {
                #pragma acc loop independent vector(16)
                for(j=0; j<n;j++)
                {
                    v[n*i+j] = 0;
                }
            }
        }
    }
    return v;
}

double** Matriz()
{
    double** M = (double**) malloc(n*sizeof(double*));
    int i,j;
    for(i=0;i<n;i++)
    {
        *(M+i) = (double*)malloc(n*sizeof(double));
    }
    #pragma acc data copyout(M[0:n][0:n])
    {
        #pragma acc region
        {
            # pragma acc loop independent vector(16)
            for(i=0;i<n;i++)
            {
                # pragma acc loop independent vector(16)
                for(j=0;j<n;j++)
                {
                    M[i][j] = 0;
                }
            }
        }        
    }
    return M;
}

double* generarV()
{
    double* v = (double*) malloc(n*sizeof(double));
    int i;
    #pragma acc data copyout(v[0:n])
    {
        #pragma acc region
        {
            #pragma acc loop independent vector(16)
            for(i=0; i<n;i++)
            {
                v[i] =  0;
            }
        }
        
    }
    return v;
}

double* funF(double* x)
{
    double* F = generarV();
    int i;
    F[0] = x[0]*(3-0.5*x[0])-2*x[1]+1;
    F[n-1] = x[n-1]*(3-0.5*x[n-1])-x[n-2]+1;
    #pragma acc data copyin(x[0:n]) copyout(F[0:n])
    {
        #pragma acc region
        {
            # pragma acc loop independent vector(16)
            for(i = 1; i<n-1;i++)
            {
                F[i] = x[i]*(3-0.5*x[i])-x[i-1]-2*x[i+1]+1;
            }
        }
    }
    return F;
}

double* funJ(double* x)
{
    double* J = generarM();
    int i;
    J[0]=3-x[0]; J[1]=-2;
    J[n*n-1]=3-x[n-1]; J[n*n-2]=-1;
    #pragma acc data copyin(x[0:n]) copyout(J[0:n][0:n])
    {
        #pragma acc region
        {
            # pragma acc loop independent vector(16)
            for(i=1;i<n-1;i++)
            {
                J[n*i+i-1]=-1; J[n*i+i]=3-x[i]; J[n*i+i+1]=-2;
            }
        }
    }
    return J;  
}

double normaP(double* v, int p)
{
    int i=0;
    double norm=.0;
    // #pragma acc region for parallel vector(32) reduction(+:norm)
    for(i=0;i<n;i++)
    {
        norm+=pow(v[i],p);
    }

    return pow(norm,1.0/p);
}

void imprimir(double* v)
{
    int i,j;
    for(i=0; i<n;i++)
    {
        for(j=0; j<n;j++)
        {
            printf("%f  ",v[n*i+j]);
        }
        printf("\n");
    }
}

void imprimirV(double* v)
{
    int i;
    for(i=0; i<n;i++)
    {
        printf("%f ",v[i]);
    }
    printf("\n");
}

void imprimirM(double** M)
{
    int i,j;
    for(i=0; i<n;i++)
    {
        for(j=0; j<n;j++)
        {
            printf("%f  ",M[i][j]);
        }
        printf("\n");
    }
}

double** parserVM(double* v)
{
    double** M = Matriz();
    int i,j;
    #pragma acc data copyin(v[0:n]) copyout(M[0:n][0:n])
    {
        #pragma acc region
        {
            # pragma acc loop independent vector(16)
            for(i=0;i<n;i++)
            {
                # pragma acc loop independent vector(16)
                for(j=0;j<n;j++)
                {
                    M[i][j] = v[n*i+j];
                }
            }
        }
    }
    return M;
}

double* parserMV(double** M)
{
    double* v = generarM();
    int i,j;
    #pragma acc data copyin(M[0:n][0:n]) copyout(v[0:n*n])
    {
        #pragma acc region
        {
            # pragma acc loop independent vector(16)
            for(i=0;i<n;i++)
            {
                # pragma acc loop independent vector(16)
                for(j=0;j<n;j++)
                {
                    v[n*i+j] = M[i][j];
                }
            }
        }
    }
    return v;
}

double* haciaAdelante(double* A, double*b)
{
    double* x = generarV();
    int i,j;
    x[0] = b[1]/A[0];
    #pragma acc data copyin(A[0:n*n],b[0:n]) copyout(x[0:n])
    {
        #pragma acc region
        {
            # pragma acc loop independent vector(16)
            for(i=1;i<n;i++)
            {
                double s = 0;
                # pragma acc loop independent vector(16) reduction(+:s)                
                for(j=0;j<i;j++)
                {
                    s+=A[n*i+j]*x[j];
                }
                x[i]=(b[i]-s)/A[(n+1)*i];
            }
        }    
    }
    return x;
}

double* haciaAtras(double* A, double*b)
{
    double* x = generarV();
    int i,j;
    x[n-1] = b[n-1]/A[n*n-1];
    #pragma acc data copyin(A[0:n*n],b[0:n]) copyout(x[0:n])
    {
        #pragma acc region
        {
            # pragma acc loop independent vector(16)
            for(i=n-2;i>=0;i--)
            {
                double s = 0;
                # pragma acc loop independent vector(16) reduction(+:s)                
                for(j=i+1;j>=0;j--)
                {
                    s+=A[n*i+j]*x[j];
                }
                x[i]=(b[i]-s)/A[(n+1)*i];
            }
        }
    }
    return x;
}

void factLU(double* A, double** L, double** U, double** P)
{
    double** _A = parserVM(A);
    double** _L = Matriz();
    double** _U = Matriz();
    double** _P = Matriz();

    mat_LU(_A, _L, _U, _P, n);

    *L = parserMV(_L);
    *U = parserMV(_U);
    *P = parserMV(_P);

    free(_A);
    free(_L);
    free(_U);
    free(_P);
}

double* MetodoLU(double *A, double* b)
{
    double* L=generarM();
    double* U=generarM();
    double* P=generarM();
    double* c=generarV();

    factLU(A,&L,&U,&P);

    cblas_dgemv(CblasColMajor, CblasNoTrans,n,n,-1,P,n,b,1,0,c,1);//-b
    // cblas_dtrsv(CblasColMajor, CblasLower,CblasNoTrans,CblasUnit,n,L,n,c,1);//forward
    double* y = haciaAdelante(L,c);
    // cblas_dtpsv(CblasColMajor, CblasUpper,CblasNoTrans,CblasUnit,n,U,c,1);//backward
    double* x = haciaAtras(U,y);

    free(L);
    free(U);
    free(P);
    free(y);
    free(c);
    return x;
}

double* sumV(double* u, double* v)
{
    double* w = generarV();
    int i;
    #pragma acc data copyin(u[0:n],v[0:n]) copyout(w[0:n])
    {
        #pragma acc region
        {
            # pragma acc loop independent vector(16)            
            for(i=0;i<n;i++)
            {
                w[i]=u[i]+v[i];
            }
        }
    }
    return w;
}

double* MetodoNewtonRaphson(double* x0, int *numIt, int *calculated)
{
    double* x=generarV();
    cblas_dcopy(n,x0,1,x,1);
    while((*numIt)<MaxIt && (*calculated)==0)
    {
        double* Fx = funF(x);
        double* Jx = funJ(x);
        double* dx = MetodoLU(Jx,Fx);
        if(normaP(dx,n)<=rTol)
        {
            (*calculated)=1;
            return x;
        }
        *numIt=*numIt+1;
        // imprimirV(x);
        // imprimirV(dx);
        x = sumV(x,dx);
        free(Jx);
        free(Fx);
        free(dx);
        if((*numIt)%10==0)
        {
            // printf("Iteracion: %d\tx: ",(*numIt));
            // imprimirV(x);
        }
    }
    return x;
}

int main()
{
    int nTest[5] = {4,5,10,15,20};
    for(int i=0; i<5;i++)
    {
        n = nTest[i];
        double* x0 = generarV();
        int calculated = 0, numIt = 0;
        StartTimer();
        double* x = MetodoNewtonRaphson(x0, &numIt, &calculated);
        double runtime = GetTimer()/1000.f;
        // printf("Solucion del sistema: x:");
        // imprimirV(x);
        printf("%g ",runtime);
        free(x0);
        free(x);
    }
    puts("");
    return EXIT_SUCCESS;
}

#endif
