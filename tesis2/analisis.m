%function [] = analisis()
    f = load('tEjecucion.txt');
    n = f(1,1:5);
    seq = f(2,1:5);
    omp = f(3,1:5);
    acc = f(4,1:5);
    hold on
    grid on
    figure(1)
    plot(n,seq,'ro-',"linewidth", 4,'DisplayName','Secuencial')
    plot(n,omp,'g+-',"linewidth", 4,'DisplayName','OpenMP')
    plot(n,acc,'b*-',"linewidth", 4,'DisplayName','OpenACC')
    xlabel('Dimension Sistema');
    ylabel('Tiempo de Ejecucion(s)');
    legend('show')
    print -djpg tEjecucion.jpeg

    spOMP=seq./omp;
    spACC=seq./acc;
    figure(2)
    hold on
    grid on
    plot(n,spOMP,'g+-',"linewidth", 4,'DisplayName','OpenMP')
    plot(n,spACC,'b*-',"linewidth", 4,'DisplayName','OpenACC')
    xlabel('Dimension Sistema');
    ylabel('SpeedUP');
    legend('show')
    print -djpg speedUPs.jpeg

    eOMP=spOMP./n;
    eACC=spACC./n;
    figure(3)
    hold on
    grid on
    plot(n,eOMP,'g+-',"linewidth", 4,'DisplayName','OpenMP')
    plot(n,eACC,'b*-',"linewidth", 4,'DisplayName','OpenACC')
    xlabel('Dimension Sistema');
    ylabel('Eficiencia');
    legend('show')
    print -djpg eficiencias.jpeg
%end
