#!/bin/bash
#NR secuencial
gcc NewtonRaphson.c -o NR -lm -lopenblas
./NR >> tEjecucion.txt
echo "results of MM are ready!"

#NR OMP
gcc NewtonRaphson_OMP.c -fopenmp -lm -lopenblas -o NR_OMP
./NR_OMP >> tEjecucion.txt
echo "results of MM_OMP are ready!"

#NR ACC
pgcc -acc NR_ACC.c -lopenblas -lm -o NR_ACC
./NR_ACC >> tEjecucion.txt
echo "results of MM_ACC are ready!"

octave analisis.m
echo "analisis is ready!"
